def stock_profits(arr):
    lowest = arr[0]
    best_profit = 0
    best_sell = arr[1]
    best_buy = arr[0]
    for price in arr:
        if price<lowest: 
            lowest = price
        profit1 = price - best_buy
        profit2 = price - lowest
        if profit1>best_profit:
            best_sell = price
            best_profit=profit1
        if profit2>best_profit:
            best_buy = lowest
            best_sell = price
            best_profit = profit2
    return (best_buy,best_sell, best_profit)

if __name__ == "__main__":
    print(__name__)
    prices1 = [4,7,3,7,2,4,1]
    prices2 = [3, 6, 9, 1, 4, 2, 8]
    prices3 = [3, 6, 9, 1, 4, 2, 8, 9]
    test = [prices1,prices2,prices3]
    for prices in test:
        print(prices, stock_profits(prices))


