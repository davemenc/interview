#largest element 2
def largest(l):
	assert len(l)>0,"list is empty"
	biggest = l[0]
	for e in l:
		if e > biggest:
			biggest = e
	return biggest

if __name__ == "__main__":
	print(largest([12345679]),largest([1,2,3,5,8,7,4,9]) )