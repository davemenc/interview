def read_logfile(fname):
	#this processes api log files
	api_calls = {}
	with open(fname,"rt") as f:
		for line in f:# get lines from the file
			data = line.strip().split(" ") #Break the line into fields
			# ["get_foo","start","2222222100"]
			assert len(data)>2 
			api_name = data[0] # "get_foo"
			api_type = data[1] # "start"
			time = data[2] #"2222222100"
			if api_name not in api_calls:
				api_calls[api_name] = [time] #{"get_foo":["2222222100"]}
			else:
				api_calls[api_name].append(time) #{"get_foo":["2222222100","2222222150"]}
	for api_name in api_calls.keys(): #"get_foo"
		start = True
		time_total = 0.0
		time_count = 0.0
		for time in api_calls[api_name]: #"2222222100"
			if start: #True
				time_total -= float(time) # -2222222100.0
				start = False
				time_count += 1 #1
			else: 
				time_total += float(time) #50.0
				start = True
		print("{}: average = {}".format(api_name,round(time_total/time_count,0))) #40
read_logfile("log")
