class Heap:
	def __init__(self,list=None):
		if list is None:
			self.heap = [0] 
			self.count = 0
		else:
			self.heap = [0]+list
			self.count = len(list) 
			self.heapify()
	def heapify(self):
		for i  in range(1,int(len(self.heap)/2)):
			if self.heap[i]>self.heap[i*2]:
				if self.heap[i*2]<self.heap[i*2+1]:
					self.heap[i],self.heap[i*2] = self.heap[i*2],self.heap[i] 
				else:
					self.heap[i],self.heap[i*2+1] = self.heap[i*2+1],self.heap[i] 
if __name__ == "__main__":
	list = [18,18,6,17,12,1,11]
	print(list)
	h = Heap(list)
	print(h.heap)
			
