import random,sys
def count_misses(tries,l):
    count = 0
    for i in l:
        count += i
    if count==0:
        print(tries,":",count)
        exit()

if len(sys.argv)<2:
    SIZE = 52
else:
    SIZE = int(sys.argv[1])
MAX_COUNT = 5000
l = [1]*SIZE

for i in range(0,MAX_COUNT):
    loc = int(random.random()*len(l))
    l[loc] = 0
    count_misses(i,l)
 
 
