import random

def shuffle_list(l):
    new_list = []
    while len(l)>0:
        pos = int(random.random()*len(l))
        new_list.append(l[pos])
        del(l[pos])
    return new_list

if __name__ == "__main__":
    mylist = [1,2,3,4,5,6,7,8,9]
    print(shuffle_list(mylist))
    

    
    
