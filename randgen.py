import sys
last_random = 16224622636
A = 9292669683099875474
B = 9451605372664442176
M = 8453098781605300014

def next_rand():
    global last_random,A,B,M
    r = (A*last_random+B) % M
    last_random = r
    return (r%sys.maxsize)/sys.maxsize
if __name__ == "__main__":
    print("max",sys.maxsize)
    for i in range(0,1000):
        print( int( round( 10*next_rand(),0)))

