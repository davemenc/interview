def count_duplicates(s):
    """ this looks for  letters duplicated 2 or 3 times; the tuple returned has values based on the doubles and triples found"""
    #print(s,type(s))
    assert type(s)==type("fo"),"count_duplicates passed non strin"
    counts = {}
    for c in s:
        if c in counts:
            counts[c]+=1
        else:
            counts[c]=1
    # now we have counts of all letters; look for letters that are 2 or 3
    count_two = 0
    count_three = 0
    for c in counts:
        if counts[c] == 2:
           count_two = 1 
        elif counts[c] == 3:
            count_three = 1
    return count_two,count_three

def test_count_duplicates():
    """ tests count_duplicates() """
    tests = {'abcdef':(0,0),'bababc':(1,1),'abbcde':(1,0),'abcccd':(0,1),'aabcdd':(1,0),'abcdee':(1,0),'ababab':(0,1)}
    for test,count in tests.items():
        assert count == count_duplicates(test),"test {} failed. Correct value is {}. Got {}.".format(test,count,count_duplicates(test))

def hash_list(box_id_list):
    """ this takes a list of tags, gets duplicat flags from each one, sums them and returns the product as a hash """
    total_two = 0
    total_three = 0
    for box_id in box_id_list:
        two,three = count_duplicates(box_id)
        total_two += two
        total_three += three
    print (total_two, total_three)
    return total_two * total_three   
        
def test_hash_list():
    test = {'abcdef','bababc','abbcde','abcccd','aabcdd','abcdee','ababab'}
    correct_result = 12
    assert hash_list(test)==correct_result,"Hash list failed. Correct result is {} and it got {}.".format(correct_result,hash_list(test))

test_count_duplicates()
test_hash_list()
PUZZLE_NO = 2
inputfile = "puzzleinput"+str(PUZZLE_NO)+".txt"

with open(inputfile,"rt") as i_file:
    box_ids = list()
    for line in i_file:
        box_ids.append(line.strip())
print(hash_list(box_ids))



