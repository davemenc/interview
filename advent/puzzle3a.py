import copy
class Cloth:
	def __init__(self,size):
		row = [0]*size
		self.cloth = list()
		for i in range(0,size):
			self.cloth.append(copy.copy(row))
		self.swatches = []
	
	def find_uncut_cloth(self):
		for s in self.swatches:
			overlap = False
			for rx in range(0,s.width):
				for ry in range(0,s.height):
					assert self.cloth[rx+swatch.locx][ry+swatch.locy]>0,"swatch {} not recorded!".format(s)
					if self.cloth[rx+swatch.locx][ry+swatch.locy] != 1:
						overlap=True
						check = 50
						if s.id>check and s.id<check+30:
							print("NO GOOD!",s,rx,ry)
						break
				if overlap:
					break
			if not overlap: 
				print(s)

	def cut_cloth(self,swatch):
		for rx in range(0,swatch.width):
			for ry in range(0,swatch.height):
				self.cloth[rx+swatch.locx][ry+swatch.locy]+=1

	def calc_cloth_overlap(self):
		total = 0
		for x in range(0,len(self.cloth)):
			for y in range(0,len(self.cloth[0])):
				if self.cloth[x][y]>1:
					total += 1
		return total	
	
class Swatch:
	total_swatch_size = 0
	def __init__(self,line):
			rec = line.strip()
			at = rec.find("@")
			colon = rec.find(":")
			self.id = int(rec[1:at-1])
			loc = rec[at+1:colon].split(",")
			self.locx = int(loc[0])
			self.locy = int(loc[1])
			size = rec[colon+1:].split("x")
			self.width = int(size[0])
			self.height = int(size[1])
			Swatch.total_swatch_size += self.width+self.height
			


	def __repr__(self):
			return repr((self.id,self.locx,self.locy,self.width,self.height))
	
		

PUZZLE_NO = 3
inputfile = "puzzleinput"+str(PUZZLE_NO)+".txt"
cloth = Cloth(1000)

with open(inputfile,"rt") as i_file:
	for line in i_file:
		swatch = Swatch(line)
		cloth.swatches.append(swatch)
		cloth.cut_cloth(swatch)
print( Swatch.total_swatch_size)
print(cloth.calc_cloth_overlap())
cloth.find_uncut_cloth()