fname = "puzzleinput1.txt"

changelist = []
with open(fname,"rt") as fp:
    for line in fp:
        changelist.append(int(line.strip()))
freqlist = set()
freq = 0
timesthrough = 0
i = 0
while freq not in freqlist:
    freqlist.add(freq)
    freq += changelist[i]
    i=i+1
    if i>len(changelist)-1:
        i=0
        timesthrough+=1
print("first duplicate: ",timesthrough,i,freq)
    
