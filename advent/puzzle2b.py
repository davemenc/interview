def count_diff(id1,id2):
    assert len(id1)==len(id2),"The two ids aren't the same length. {} vs {}".format(id1,id2)
    count = 0
    for i in range(0,len(id1)):
        if id1[i]!=id2[i]:
            count += 1
            idx = i
            if count > 1: # 
                break

    return count,idx

PUZZLE_NO = 2
inputfile = "puzzleinput"+str(PUZZLE_NO)+".txt"

with open(inputfile,"rt") as i_file:
    box_ids = list()
    for line in i_file:
        box_ids.append(line.strip())
for i in range(0,len(box_ids)):
    for j in range(i+1,len(box_ids)):
        count,idx =  count_diff(box_ids[i],box_ids[j])
        if count == 1:
            print("The two we want are")
            print(box_ids[i])
            print(box_ids[j])
            diff1 = box_ids[i][:idx]+box_ids[i][idx+1:]
            diff2 = box_ids[j][:idx]+box_ids[j][idx+1:]
            print("idx:",idx)
            print(diff1)
            print(diff2)
            assert diff1==diff2,"index is {}".format(idx)
            print(box_ids[i][:idx]+box_ids[i][idx+1:])

