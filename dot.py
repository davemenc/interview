class Dot:
    def __init__(self,g):
        self.graph = g
        self.dot = None
        self.convert_graph_to_dot();

    def convert_graph_to_dot(self):
        dot = "graph G { \n"
        for edge in self.graph:
            node1 = edge[0]
            node2 = edge[1]
            dot += '\t"{}" -- "{}";\n'.format(node1,node2)
        dot += '}\n'
        self.dot = dot

g = [('main', 'parse'), ('parse', 'execute'), ('main', 'init'), ('main', 'cleanup'), ('execute', 'make_string'), ('execute', 'printf'), ('init', 'make_string'), ('main', 'printf'), ('execute', 'compare')]
d = Dot(g)
print(d.dot)
#to convert this to png: dot -Tpng graph.dot -o graph.png
