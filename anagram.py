str1 = "abcdeedcba"
str2 = "abcdegedcba"
str3 = "abcdef"
def anagram(str):
    str_len = len(str)
    for i in range(0,str_len//2):
        if str[i] != str[str_len-i-1]:
            return False
    return True
print(str1,anagram(str1))
print(str2,anagram(str2))
print(str3,anagram(str3))