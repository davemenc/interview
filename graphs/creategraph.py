import random
def add_node(graph,node):
    newnode = len(graph)
    graph[node].append(newnode)
    graph.append([])
    graph[newnode].append (node)
def connect_nodes(graph,node1,node2):
	if node1>=len(graph) or node2>=len(graph):#node1 or 2 are bogus
		return #so give up
	if node1 not in graph[node2]:
		graph[node2].append(node1)
	if node2 not in graph[node1]:
		graph[node1].append(node2)

def pick_node(graph):
    return random.randint(0,len(graph)-1)

def create_loops(graph,loopcount):
	#we're going to try to connect nodes with only 1 link to each other
	#first, get a list of single-list nodes
	singles = []
	for i in range(0,len(graph)):
		if len(graph[i])==1:
			singles.append(i)
	#now randomize
	random.shuffle(singles)
	#fix the goal so it's possible
	if loopcount>len(singles)/2: 
		loopcount = int(len(singles)/2)
	#now get the top loopcount nodes and connect them
	for i in range(0,loopcount):
		connect_nodes(graph, singles[i],singles[loopcount+i])

MAXNODES = 10
MAXLOOPS = 3

graph = [[]]
for i in range(0,MAXNODES):
    n = pick_node(graph)
    add_node(graph,n)
create_loops(graph,MAXLOOPS)

print(graph)
