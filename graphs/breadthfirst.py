graph = [[1, 3], [0, 2], [1, 4, 8], [0, 5, 6], [2, 7], [3, 10], [3, 7], [4, 6], [2, 9], [8, 10], [5, 9]]
print("Our graph: {}".format(graph))
start = 0
goal = 10
loopcount = 0

nodes = []

finishedpaths = []
paths = [[start]] # a path is a list of nodes; paths is a list of incomplete paths
while len(paths)>0 and loopcount<100:
    loopcount += 1
    print("Lc={}; len(paths):{}; len(finished): {}".format(loopcount,len(paths),len(finishedpaths))) 
    path = paths[-1] # get the last incomplete path
    del(paths[-1])
    curnode = path[-1] # get the last node in this path
    for n in graph[curnode]: # get the connections from this
        nodes.append(n)
        if n not in path: # if it already has n then this is a loop
			newpath = path + [n]
			if n == goal: #Good path: add it to list
				finishedpaths.append(newpath)
			else:
				if len(newpath)<=len(graph): #look: a path can't be longer than the graph!
					paths.append(newpath)
if len(finishedpaths)==0:
	print("WE didn't find a path!")
bestlen = len(graph)+1
bestpath = None
for path in finishedpaths:
	if len(path)<bestlen:
		bestpath = path
		bestlen = len(path)
print("Path from {} to {}: {}".format(start,goal,bestpath))
print("Path len: {}".format(bestlen))
print("Loopcount = {}".format(loopcount))
print()

	




