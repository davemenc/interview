graph = [[1, 3], [0, 2], [1, 4, 8], [0, 5, 6], [2, 7], [3, 10], [3, 7], [4, 6], [2, 9], [8, 10], [5, 9]]
print("Our graph: {}".format(graph))
start = 0
goal = 10
loopcount = 0

nodes = []

paths = [[start]] # a path is a list of nodes; paths is a list of incomplete paths
while len(paths)>0 and loopcount<100:
    loopcount += 1
    print("Lc={}; len(paths):{}".format(loopcount,len(paths))) 
    path = paths[-1] # get the last incomplete path
    del(paths[-1])
    curnode = path[-1] # get the last node in this path
    for n in graph[curnode]: # get the connections from this
        nodes.append(n)
        if n not in path: # if it already has n then this is a loop
			newpath = path + [n]
			if n == goal: #depth first search so we're done!
				print("Path from {} to {}: {}".format(start,goal,newpath))
				print("Path len: {}".format(len(newpath)))
				print("Loopcount = {}".format(loopcount))
				exit()
			else:
				if len(newpath)<=len(graph): #look: a path can't be longer than the graph!
					paths.append(newpath)
print("We never found the goal!")
print("paths",paths)
print("nodes",nodes)
print()



