#leap years
#https://docs.google.com/document/d/17CXa5MkZwY0zbTyO1yolL800hOAqCSM-7HV1zcwJp4U/edit#heading=h.j22fbaq9n26r
#Write a program that prints the next 20 leap years.

#leap year is defined as a year divisible by 4 unless divisible by 100 except for years divisible by 400 which are included
#We want to start before 2000
# I made it 30 so we'd go through 100+ years; it gets 2000 but not 2100

LEAP_YEAR_COUNT_LIMIT = 30
FIRST_YEAR = 1996
leap_year_count = 0
this_year = FIRST_YEAR
while leap_year_count < LEAP_YEAR_COUNT_LIMIT and this_year<3000 :
	if this_year % 4 == 0 and (this_year % 100 != 0 or this_year % 400 == 0):
		leap_year_count +=1
		print(leap_year_count, this_year)
	this_year += 1
