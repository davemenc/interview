import random
def how_long():
    limit = 100000
    for i in range(0,limit):
        if random.randrange(0,100)==0:
            return i
    return limit+1
def mc_100():
    limit = 100000
    counts = {}
    for i in range(0,limit):
        c = how_long()
        if c not in counts:
            counts[c] = 1
        else:
            counts[c]+=1
    for c,tot in counts.items():
        print("out of {} tries, {} happned in {} years. That's {} percent".format(limit,tot,c, round(float(tot)*100.0/float(limit),1)))
mc_100()
