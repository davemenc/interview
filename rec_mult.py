#recursive multiply
# multiply two positive integers without using the * operator. 
# without recursion...

def mult(a,b):
	print(" mult",a,b)
	if a<1 or b<1:
		return 0
	big = max(a,b)
	original_big = big
	little = min(a,b)
	original_little = little
	if little == 1:
		return big
	while little>1:
		print(" 1",big,little)
		big = big << 1
		little = little >> 1
	print(" 2",big,little)
	if original_little % 2 != 0:
		big+=original_big
	print(" 3" ,big,little)
	return big
	
	
tests = [(2,3,6),(0,4,0),(1,1,1),(5,4,20),(7,4,28),(4,15,60),(12,13,156),(41,20,820),(41,30,1230),(41,40,1640)]
for test in tests:
	a = test[0]
	b = test[1]
	e_result = test[2]
	a_result = mult(a,b)
	print("result",a,b,e_result,a_result)