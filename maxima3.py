def local_maxima(data):
    local_maximums = []
    arr = [-1000000]+data+[-1000000]
    for i in range(1,len(arr)-1):
        if arr[i]>arr[i-1] and arr[i]>=arr[i+1]:
            local_maximums.append(arr[i])
    return local_maximums
t1 = [3,8,3,8,1,5,4,1,2,7]
t2 = [5,1,6,5,5,2,3]
t3 = [1,5,7,2,2,8,7]
t4 = [7,8,4,5,4,4,10,3,2,9]
t5 = [2,2,2,2,2,2]
t6 = [1,2,3,4,5,4,3,2,1]
t7 = [1,2,1,2,1]
t8 = [1,2,2,2,1,5,1]
testlist = t8
result = local_maxima(testlist)
print(testlist)
print(result)

