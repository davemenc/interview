#Largest Element
#Write a function that returns the largest element in a list.

l = [1,5,6,7,38,8,3,4,9,12]

def largest_by_sorting(num_list):
	num_list.sort()
	return num_list[-1]

def largest_by_searching(num_list):
	largest = -1
	for i in num_list:
		if i>largest:
			largest=i
	return largest
	
print("largest_by_sorting",largest_by_sorting(l))
print("largest_by_searching",largest_by_searching(l))