def reverse_arr(arr):
    l = len(arr)
    for i in range(0,l/2):
        left = i
        right = l-i-1
        arr[left],arr[right] = arr[right],arr[left]
    return arr

print(reverse_arr([1,2,3,4,5]))
print(reverse_arr([1,2,3,4]))
