import random
#hi lo guess 2
MAX_NUMBER = 100
MAX_TRIES = 10
print("HI LO GUESS\nThis is a guessing game.")
print("I'm thinking of a number between 1 and {}. You guess what it is and I'll tell you if you are high or low.\n You have {} tries.".format(MAX_NUMBER,MAX_TRIES))
print()
number = random.randint(1,100)
for tries in range(0,10):
	guess = input("What is your guess? ")
	try:
		guess = int(guess)
	except:
		print("That is not a number. You lose")
		exit()
	if guess == number:
		print("You are right! You win!")
		exit()
	elif guess > number:
		print("Your guess is too HIGH.")
	else: 
		print("Your guess is too LOW.")
print("I'm sorry but you didn't get the answer in {} guesses. You lose.".format(MAX_TRIES))


	
