primes = [2]
for i in range(3,10000):
    is_prime = True
    for prime in primes:
        if i%prime == 0:
            is_prime=False
            break
    if is_prime:
        primes.append(i)
print(primes)

