class Word_Count:
    def __init__(self,word):
        self.word = word
        self.count = 1

    def add_count(self,count=1):
        self.count += count

    def display(self):
        print("\t".self.word,":",self.count)

class Next_Word_List:
    def __init__(self,newword):
        self.word = newword
        self.next_word_list = dict()
    
    def add_word(self, word):
        if word in self.next_word_list:
            self.next_word_list[word].add_count()
        else:
            self.next_word_list[word] = Word_Count(word)
    def display(self):
        print(self.word)
        for word_count in self.next_word_list:
            word_count.display()
        
class Master_Word_List:
    def __init__(self):
        self.m_wordlist = dict()

    def add_word_pair(self,first,second):
        if first in self.m_wordlist:
           self.m_wordlist[first].add_word(second)
        else:
            self.m_wordlist[first] = Next_Word_List(second)
    def display(self):
        for wordlist in self.m_wordlist:
            wordlist.display()

if __name__ == "__main__":
    word_list = ["the","quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog"]
    master = Master_Word_List()
    first = ""
    for word in word_list:
        second = word
        master.add_word_pair(first,second)
        first = second
    master.display()




