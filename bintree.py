from node import Node

class Bintree:
	def __init__(self):
		self.head = None

	def add(self, val):
		curnode = self.head
		newhead = None
		while curnode is not None:
			newhead = curnode
			curval = curnode.payload
			if curval == val: 
				return 
			elif curval > val:
				curnode = curnode.right
			elif curval < val:
				curnode = curnode.left
		if newhead is None:
			self.head = Node(val)
		elif newhead.payload > val:
			newhead.right = Node(val)
		else:
			newhead.left = Node(val)

	def display_tree_desc(tree):
		if tree is not None:
			Bintree.display_tree_desc(tree.left)
			print(tree.payload)
			Bintree.display_tree_desc(tree.right)
	def display_tree_asc(tree):
		if tree is not None:
			Bintree.display_tree_asc(tree.right)
			print(tree.payload)
			Bintree.display_tree_asc(tree.left)

	def display_asc(self):
		Bintree.display_tree_asc(self.head)
	def display_desc(self):
		Bintree.display_tree_desc(self.head)


if __name__ == "__main__":
	values = [10, 6, 8, 3, 5, 8, 7,14,13,9,19,2,29,11,18]
	values = [10, 5,2,3,4,1,6,-1,-6, -8, -3, -5, 8, -7,4,3,9,9,-2,9,1,8,7,8,7,3]

	print ("values",values)
	tree1 = Bintree()
	print("______\nAscending")
	for v in values:
		tree1.add(v)

	tree1.display_asc()
	print("________\nDescending")
	tree1.display_desc()
