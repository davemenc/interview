class Stackofstacks:
	def __init__(self,max=6):
		self.stacks = [[]]
		self.maxsize = max


	def push(self,val):
		if len(self.stacks) == 0:
			self.stacks.append([])
		if len(self.stacks[-1]) >= self.maxsize:
			self.stacks.append([val])
		else:
			self.stacks[-1].append(val)

	def __repr__(self):
		s = ""
		for n,stack in enumerate(self.stacks):
			s += "{} : {}\n".format(n,stack)
		return s	
	def pop(self):
		if len(self.stacks)==0:
			return None
		result = self.stacks[-1][-1]
		del(self.stacks[-1][-1])
		if len(self.stacks[-1])==0:
			del self.stacks[-1]
		return result
				

if __name__ == "__main_" or True:
	values = [16,7,2,8,20,1,9,5,14,9,3,14,19]
	print(values)
	sos = Stackofstacks()
	for v in values:
		sos.push(v)
	print(sos)
	print ("__________")
	v = sos.pop()
	print(v)
	while v is not None:
		v = sos.pop()
		print(v)
		print(sos)


	
	for v in values:
		sos.push(v)
	print(sos)

		

