def find_common_el(arr1,arr2,arr3):
    common = []
    while min(len(arr1),len(arr2),len(arr3))>0:
        val1 = arr1[0]
        val2 = arr2[0]
        val3 = arr3[0]
        if val1 == val2 and val2 == val3:
            common.append(val1)
        min_val = min(val1,val2,val3)
        if min_val == val1:
            del(arr1[0])
        if min_val == val2:
            del(arr2[0])
        if min_val == val3:
            del(arr3[0])
    return common
if __name__ == "__main__":
            
    arr1 = [1, 5, 10, 20, 40, 80]
    arr2 = [6, 7, 20, 80, 100]
    arr3 = [3, 4, 15, 20, 30, 70, 80, 120]
    #Output: 20, 80
    print(find_common_el(arr1,arr2,arr3))
    print("should be [20,80]")

