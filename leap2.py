def leap_test(year):
	if year % 4 != 0:
		return False #if it's not divisible by 4 it's NEVER a leap year
	if year % 400 == 0:
		return True # if it's divisible for 400 it's ALWAYS a leap year
	if year % 100 == 0:
		return False # if it's divisible by 100 but not 400 it's NOT a leap year
	return True # everything left is a leap year
	
if __name__ == "__main__":
	for y in range(1999,2105):
		if leap_test(y):
			print(y)
			