def find_stock_prices(arr):
    smallest_i = 0
    best_buy_i = 0
    best_sell_i = 1
    best_profit = arr[best_sell_i] - arr[best_buy_i]        
    for i in range(0,len(arr)):
        if arr[i]-arr[smallest_i] > best_profit:
            best_sell_i = i
            best_buy_i = smallest_i
            best_profit = arr[best_sell_i] - arr[best_buy_i]        
        if arr[i] < arr[smallest_i]:
            smallest_i = i
    return(arr[best_buy_i],arr[best_sell_i])

prices = [3, 6, 9, 1, 4, 2, 8]
print(prices,find_stock_prices(prices))