def parseline(line):
  data = line.strip().split("\t")
  return data[2]
def parsefile(fname,count):
  ips = {}
  with open(fname,"rt") as f:
    for line in f:
      ip = parseline(line)
      if ip in ips:
        ips[ip] += 1
      else:
        ips[ip] = 1
  s_ip_list = sorted(ips.items(),key=lambda kv: kv[1],reverse=True)
  
  out_len = min(len(s_ip_list),count)
  print(len(s_ip_list))
  #print(s_ip_list[0:10],ips)
  for i in range(0,out_len):
    print(s_ip_list[i][1],s_ip_list[i][0])
if __name__ == "__main__":
  parsefile("log2.txt",27)

