#Pig Latin
# "The quick brown fox" becomes "Hetay uickqay rownbay oxfay".

def pig_latin(s):
	words = s.lower().split(" ")
	piglatin = ""
	for word in words:
		first = word[0]
		body = word[1:]
		piglatin += body + first + "ay "
	return piglatin
	
if __name__ == "__main__":
	tests = {"The quick brown fox":"hetay uickqay rownbay oxfay ","Now is the time that try mens souls":"ownay siay hetay imetay hattay rytay ensmay oulssay " }
	for sentence,piglatin in tests.items():
		print(sentence,pig_latin(sentence))
		if piglatin != pig_latin(sentence):
			print("ERROR")
			print(piglatin)
			print(pig_latin(sentence))
	
	

