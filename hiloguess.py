#hilo guessing game
# https://docs.google.com/document/d/17CXa5MkZwY0zbTyO1yolL800hOAqCSM-7HV1zcwJp4U/edit#heading=h.uhv9jai0ylf
import random
MAX_VALUE = 100
MAX_GUESSES = 10
print("Hi Lo Guessing Game")
print("I'm thinking of a number between 1 and 100.")
print("You guess a number and I'll tell you whether it's too high or too low.")
print("Are you ready? Oh, you're ready...")
print()
secret_number = int(random.random()*MAX_VALUE)+1
tries = 0
guess = None
while True:
	guess = input("What is your guess? ")
	tries += 1
	try:
		guess = int(guess)
	except : 
		print("The guess needs to be a number, friend.")
		print()
		continue
	if tries > MAX_GUESSES:
		print("Sorry, you took more than {} guesses. You lose. Better luck next time. ".format(MAX_GUESSES))
		exit()
	if guess > secret_number:
		print("That was too high. Try again.")
	elif guess < secret_number:
		print("That was too low. Try again.")
		print()
	else: 
		print("Congratulations! You got it in {} tries. Good job.".format(tries))
		exit()