import random
def local_maxima(arr):
    temp = [-1]+arr+[-1]
    result = []
    for i in range(1,len(temp)-1):
        #print(i,temp[i])
        if temp[i]>temp[i-1] and temp[i]>=temp[i+1]:
            result.append(i-1)
    return result

t1 = [3,8,3,8,1,5,4,1,2,7]
t2 = [5,1,6,5,5,2,3]
t3 = [1,5,7,2,2,8,7]
t4 = [7,8,4,5,4,4,10,3,2,9]
t5 = [2,2,2,2,2,2]
t6 = [1,2,3,4,5,4,3,2,1]
t7 = [1,2,1,2,1]
t8 = [3,1,5,4,7,7]
t9 = []
for i in range(10):
    t9.append(random.randint(1,10))
testlist = t2
result = local_maxima(testlist)
print(testlist)
for i in result:
    print(testlist[i])
