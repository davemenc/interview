import random

def create_deck(size):
    l = [i for i in range(size)]
    return l

def show_card(card_no):
    suits = [" of Clubs", " of Diamonds", " of Hearts", " of Spades"]
    names = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"]
    suit_no = int(card_no/len(names))
    name_no = card_no%len(names)
    return names[name_no]+suits[suit_no]

def print_deck(deck):
    pos = 0
    for card in deck:
        print(pos,show_card(card),card)
        pos += 1


def shuffle_deck(deck):
    SWAP_TIMES = len(deck)*2
    for i in range(0,SWAP_TIMES):
        loc1 = int(random.random()*len(deck))
        loc2 = int(random.random()*len(deck))
        t1 = deck[loc1]
        deck[loc1] = deck[loc2]
        deck[loc2] = t1
    return deck
        
if __name__ == "__main__":
    random.seed(0) # for testing, have it do the same thing every time
    deck = create_deck(52)
    print("unsorted")
    print_deck(deck)
    deck1 = shuffle_deck(deck)
    print("__________ SORT 1 _________")
    print_deck(deck1)
    random.shuffle(deck)
    #print("_________ SORT 2 ___________")
    #print_deck(deck#)

