def find_data(line):
	line += " " #add a "terminating" space to the end
	status = "s" # s is space, d is data
	cur_data=""
	data_record = []
	for c in line:
		if c==" " and status == "d": # end of a word
			data_record.append(cur_data)
			cur_data = ""
			status = 's'
		elif c != " " and status == "d": # middle of a word
			cur_data += c
		elif c != ' ' and status == 's': # start of a word
			cur_data += c
			status = 'd'
	return data_record

fname = 'mayordata.txt'
names = []
votes = []
pre = []

with open(fname,"rt") as fp:
	for line in fp:
		if len(line)==0:
			continue
		data = find_data(line.strip())
		if len(data) == 0:
			continue
		print("data",data)
		if data[0] == "Candidate":
			print("candidate")
			continue # no data in Candidate line
		elif data[0] == 'Precinct':
			print("Precinct")
			for i in range(1,len(data)):
				names.append(data[i])
				print(names)
		else: #vote counts
			print("else")
			pre.append(data[0])
			row = []
			for i in range(1,len(data)):
				row.append(data[i])
			votes.append(row)
print("names",names)
print("precinct",pre)
print("votes",votes)
