import random
def local_maxima(graph):
    result = []
    cval = -1
    cidx = -1
    have_candidate = False
    graph.append(-2)
    for i in range(0,len(graph)):
        if graph[i]>=cval:
            cval = graph[i]
            cidx = i
            have_candidate = True
        elif have_candidate:
            result.append(cidx)
            have_candidate = False
            cval = graph[i]
            cidx = i
    del(graph[-1])
    return result

t1 = [3,8,3,8,1,5,4,1,2,7]
t2 = [5,1,6,5,5,2,3]
t3 = [1,5,7,2,2,8,7]
t4 = [7,8,4,5,4,4,10,3,2,9]
t5 = [2,2,2,2,2,2]
t6 = [1,2,3,4,5,4,3,2,1]
t7 = [1,2,1,2,1]

t8 = [3,1,5,4,7,7]
t9 = []
for i in range(10):
    t9.append(random.randint(1,10))
testlist = t9
result = local_maxima(testlist)
print(testlist)
for i in result:
    print(testlist[i])
