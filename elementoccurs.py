#Element Occurs
#https://docs.google.com/document/d/17CXa5MkZwY0zbTyO1yolL800hOAqCSM-7HV1zcwJp4U/edit#heading=h.8h7dy1d152fg
# Write a function that checks whether an element occurs in a list.

def check_for_element(element,list):
	for el in list:
		if el == element:
			return True
	else:
		return False

if __name__ == "__main__":
	list = [1,2,3,4,5,6,7,8,9,]
	test1 = 1
	test2 = 12
	print("method 1",test1,list,test1 in list)
	print("method 1",test2,list,test2 in list)
	print("method 2",test1,list,check_for_element(test1,list))
	print("method 2",test2,list,check_for_element(test2,list))		