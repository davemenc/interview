from node import Node

class Linklist: 
	def __init__(self):
		self.head = None

	def add_end(self,val):
		newnode = Node(val)
		newnode.next = None
		if self.head is None:
			self.head = newnode
		else: 
			cur = self.head		 	
			while cur.next is not None:
				cur = cur.next 
			cur.next = newnode	
		
	def add_start(self,val):
		newnode = Node(val)
		newnode.next = self.head
		self.head = newnode

	def find(self,val):
		if self.head is None:
			return None
		cur = self.head
		while cur is not None:
			if cur.payload == val:
				return cur
			cur = cur.next
		return None	

	def __repr__(self):
		s = ""
		if self.head is None:
			return s
		cur = self.head
		while cur is not None: 
			s += "{}; ".format(cur.payload)
			cur = cur.next
		return s

if __name__ == "__main__":
	values = [4,9,10,23,1, -1, 0, None]
	list1 = Linklist()
	print(list1)

	for v in values:
		list1.add_start(v)
	print(list1)
	list2 = Linklist()
	for v in values:
		list2.add_end(v)
	print(list2)

	values.append(244)

	for v in values:
		n = list1.find(v)
		if n is None:
			print(v,n)
		else: 
			print (v,n.payload)




