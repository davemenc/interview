class Node:
	def __init__(self, payload = None):
		self.next = None
		self.prev = None
		self.left = None
		self.right = None
		self.links = []
		self.payload = payload


