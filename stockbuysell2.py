def stock_profits(arr):
    lowest = arr[0]
    best_profit = 0
    best_sell = arr[1]
    best_buy = arr[0]
    for price in arr:
        if price<lowest:
            lowest = price
        profit1 = price - best_buy
        profit2 = price - lowest
        if profit1>best_profit:
            best_sell = price
            best_profit=profit1
        if profit2>best_profit:
            best_buy = lowest
            best_sell = price
            best_profit = profit2
    return (best_buy,best_sell, best_profit)

prices = [3, 6, 9, 1, 4, 2, 8]
print(prices,stockprofits(prices))