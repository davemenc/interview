mapname = "map.txt"
map_array = [[]]
map_column=0

def get_map_val(map_array,coord):
    i = int(coord/10)
    j = coord-i*10-1
    i += -1
    print(coord,i,j)
    return(map_array[i][j])
with open(mapname,"rt") as fp:
    for line in fp:
        line = line.strip()
        if line=="+-------------------------+" or line== "+----+----+----+----+-----¦" or line == "":
            continue #skip walls and blank lines
        cells = line.split("¦")
        for cell in cells:
            cell_val = cell.strip()
            if cell_val == "":
                continue #skip blanks
            map_array[map_column].append(int(cell_val))
        map_column +=1
        map_array.append([])
print(get_map_val(map_array,11))
print(get_map_val(map_array,34))
coord = 11
for i in range(0,10):
    print(coord)
    old_coord = coord
    coord = get_map_val(map_array,old_coord)
    if old_coord == coord:
        print("Answer: ",coord)
        exit()
