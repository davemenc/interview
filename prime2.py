primes = [2]
for i in range(3,10000000):
    if i % 1000 == 0:
        l = len(primes)
        p = round(float(l)/float(i),4)
        print("{}\t{}\t{}".format(i,l,p))
    is_prime = True
    for prime in primes:
        if prime*prime > i:#we've checked enough
            break
        elif  i % prime == 0:
            is_prime=False
            break
    if is_prime:
        primes.append(i)

