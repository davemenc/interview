def linear_merge(l1,l2):
    i = 0
    j = 0
    result = []
    while i<len(l1) and j<len(l2):
        if l1[i] < l2[j]:
            result.append(l1[i])
            i += 1
        else:
            result.append(l2[j])
            j += 1
    for el in l1[i:]:
        result.append(el)
    for el in l2[j:]:
        result.append(el)
    return result

l1 = [5,2,4,7,2]
l1.sort()
l2 = [9,1,2,3,4,5]
l2.sort()
print("l1",l1)
print("l2",l2)
print(linear_merge(l1,l2))
