vals = [8,3,9,2,0,0,2,8,4,7,8,3,0,9,2]
def local_minima(arr):
    min_list = []
    loc_arr = arr.copy()
    loc_arr = [1000000]+arr+[10000] # note: assumes first and last elements are potential min
    for i in range(1,len(loc_arr)-1):# skip first and last entries 
    	if loc_arr[i] < loc_arr[i+1] and loc_arr[i-1] >= loc_arr[i]:
    	    min_list.append(loc_arr[i])
    return(min_list)
print(vals,local_minima(vals))
