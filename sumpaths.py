from node import Node

class Bintree:
	def __init__(self):
		self.head = None

	def add(self, val):
		curnode = self.head
		newhead = None
		while curnode is not None:
			newhead = curnode
			curval = curnode.payload
			if curval >= val:
				curnode = curnode.right
			elif curval < val:
				curnode = curnode.left
		if newhead is None:
			self.head = Node(val)
		elif newhead.payload > val:
			newhead.right = Node(val)
		else:
			newhead.left = Node(val)

	def display_tree_desc(tree):
		if tree is not None:
			Bintree.display_tree_desc(tree.left)
			print(tree.payload)
			Bintree.display_tree_desc(tree.right)
	def display_tree_asc(tree):
		if tree is not None:
			Bintree.display_tree_asc(tree.right)
			print(tree.payload)
			Bintree.display_tree_asc(tree.left)

	def display_asc(self):
		Bintree.display_tree_asc(self.head)
	def display_desc(self):
		Bintree.display_tree_desc(self.head)

def add_q(q,obj):
	q.append(obj)

def next_q(q):
	result = q[-1]
	del(q[-1])
	return result

def show_paths(name,paths):
	print("\n{}\n".format(name))
	for i,p in enumeration(paths):
		print ("{}: ".format(i))
		for i in range(1,len(p)):
			print(" {};".format(p[i]))
			
			
def count_paths(tree,goal):
	q = []
	paths = []
	goodpaths = []
	count = 0

	first_list = [tree.head.payload,tree.head]
	add_q(q,first_list)
	while len(q)>0:
		path = next_q(q)
		paths.append(path)
		if path[0] == goal:
			goodpaths.append(path)
		last_node = path[-1]
		if last_node.left is not None:
			add_q(q,[last_node.left.payload,last_node.left])
			leftpath = path.copy()
			leftpath.append(last_node.left)
			leftpath[0] += last_node.left.payload
			add_q(q,leftpath)
		if last_node.right is not None: 
			add_q(q,[last_node.right.payload,last_node.right])
			rightpath = path.copy()
			rightpath.append(last_node.right)
			rightpath[0] += last_node.right.payload
			add_q(q,rightpath)

	show_paths("paths",paths)
	show_paths("goodpaths",goodpaths)
	show_paths("count",count)






	
	
if __name__ == "__main__":
	values = [10, 5,2,3,4,1,6,-1,-6, -8, -3, -5, 8, -7,4,3,9,9,-2,9,1,8,7,8,7,3]
	print ("values",values)
	tree1 = Bintree()
	for v in values:
		tree1.add(v)
	#tree1.display_asc()
	count_paths(tree1,5)	
		

		
