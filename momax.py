# python to find local max and plateau in list of integers

list1 = [0,1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1,1,2,3,2,1,0,1,2,3,4,5,5,5,4,3,2,1,0]

maxs = []
mins = []
plats = []

# Testing with these:
lista = [1,1,1,1,1,1,1,1,1,1,1]
listb= [0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0]

# Find the length of the list, make this the stopping piont 
# iterate through the list one element at a time
# A max =  the current element at position n is more then n-1 AND more than n+1 is 

print (list1)

for n in range(0, len(list1)):
	#Only run when we are not in first or last position, 0 to n-1
	#print("List element:", list1[n] , n )
	if (n != 0) and ( n != len(list1)-1):
		# Then a max bigger than previous and next element
		if ( list1[n-1] < list1[n] ) and ( list1[n] > list1[n+1] ):
			maxs.append( list1[n])
		#Just change the > sign in the second part to == for finding Plateau 
		elif ( list1[n-1] < list1[n] ) and ( list1[n] == list1[n+1] ):
			plats.append( list1[n])
		#local mins are less than neighbors
		elif ( list1[n-1] > list1[n] ) and ( list1[n] < list1[n+1] ):
			mins.append( list1[n])

print ("Length       : ", len(list1))
print ("Global Max   : ", max(list1))
print ("Global Min.  : ", min(list1))

print ("Local Maxs 	:", maxs)
print ("Local plats	:", plats)
print ("Local mins	:", mins)
