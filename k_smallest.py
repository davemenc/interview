def find_largest(l):
    largest = -100000
    for i in range(0,len(l)):
        if l[i]>largest:
            largest = l[i]
            l_index = i
    return largest,l_index
def k_smallest(arr,k):#O((n-k))*N)
    print("k_smallest:",arr,k)
    assert(len(arr)>=k)
    result = arr[0:k]
    m_el = -1
    m_el,m_idx = find_largest(result)
    for i in range(k,len(arr)):
        if arr[i]<m_el:
            m_el = arr[i]
            result[m_idx] = arr[i]
            m_el,m_idx = find_largest(result)
    return result
def k_smallest_bubble(arr,k): #O(k*N)
    for i in range(0,k):
        smallest = arr[i]
        for j in range(i,len(arr)):
            if smallest>arr[j]:
                smallest = arr[j]
                arr[i],arr[j] = arr[j],arr[i] # swap i and j elements
    return arr[0:k]


    

l = [1, 7, 4, 5, 9, 6, 2, 5, 3, 4]
print("k_smallest",k_smallest(l,4))
print("k_smallest_bubble",k_smallest_bubble(l,4))

